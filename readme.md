Domain | Method    | URI                 | Name          | Action                                             | Middleware |
+--------+-----------+---------------------+---------------+----------------------------------------------------+------------+
|        | GET|HEAD  | /                   |               | App\Http\Controllers\HomeController@index          | web        |
|        | GET|HEAD  | media               | media.index   | App\Http\Controllers\Media\MediaController@index   | web        |
|        | POST      | media               | media.store   | App\Http\Controllers\Media\MediaController@store   | web        |
|        | GET|HEAD  | media/create        | media.create  | App\Http\Controllers\Media\MediaController@create  | web        |
|        | GET|HEAD  | media/{medium}      | media.show    | App\Http\Controllers\Media\MediaController@show    | web        |
|        | PUT|PATCH | media/{medium}      | media.update  | App\Http\Controllers\Media\MediaController@update  | web        |
|        | DELETE    | media/{medium}      | media.destroy | App\Http\Controllers\Media\MediaController@destroy | web        |
|        | GET|HEAD  | media/{medium}/edit | media.edit    | App\Http\Controllers\Media\MediaController@edit    | web   
