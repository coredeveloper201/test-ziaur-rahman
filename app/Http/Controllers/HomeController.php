<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use DB;
use Illuminate\Support\Facades\File;
use Storage;
use Log;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {  
       
        
     return view('welcome');
       

    }



//delete data in demo version
    public function clearDemoDatabaseDaily()
    {
        if (env('APP_ENV') == 'demo') {

            // migrate all table
            Artisan::call('migrate:refresh', [
                '--force' => true,
            ]);

            //seed all table
            Artisan::call('db:seed', [
                '--force' => true,
            ]);
            Log::info('Demo data deleted at', [date('d-m-Y h:i:s:A')]);
        }
    }

    public function download_sample()
    {
        return Response::download(base_path('sample_email_list.csv'));
    }

    public function clearLog()
    {
       $f = @fopen(storage_path('logs/laravel.log'), "r+");
       if ($f !== false) {
           ftruncate($f, 0);
           fclose($f);
       }
        Log::error('Clear log');
    }
}
